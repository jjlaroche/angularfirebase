import { Component, OnInit } from '@angular/core';

import { UsuarioService } from '../../servicios/usuario.service';
import { Router } from '@angular/router';

import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email: any ;
  clave: any;

  constructor(private usuarioService: UsuarioService, public router: Router, public toastrService: ToastrService) {

   }

  ngOnInit() {
  }

  onSubmitLogin() {
    this.usuarioService.loginUsuario(this.email, this.clave)
    .then((resultado) => {
      this.toastrService.success('Log in exitoso', 'Operacion Exitosa');
      this.router.navigate(['/privado']);
    }).catch((error) => {
      console.log('por' + error);
      this.toastrService.warning('No se pudo realizar el login', 'Ops algo salió mal');
      this.router.navigate(['/login']);
    });
  }

  loginGoogle() {
    console.log('Login Google');
    this.usuarioService.loginGoogle().then( resultado => {
      this.router.navigate(['/privado']);
    }).catch( error => {
      console.log(error);
    });
  }

  loginFacebook() {
    console.log('Login Facebook');
    this.usuarioService.loginFaceBook().then(resultado =>{
      this.router.navigate(['/privado']);
    }).catch (error => {
      console.log(error);
    });
  }

}
