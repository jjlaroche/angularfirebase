import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../../servicios/usuario.service';

import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-navbarpage',
  templateUrl: './navbarpage.component.html',
  styleUrls: ['./navbarpage.component.css']
})
export class NavbarpageComponent implements OnInit {

  isLogin: boolean;
  nombre: string;
  email: string;
  foto: string;

  constructor(private usuarioService: UsuarioService, private toastrService: ToastrService) { }

  ngOnInit() {
    this.usuarioService.getInfoCuenta().subscribe(resultado => {
      if (resultado) {
        this.isLogin = true;
        this.nombre = resultado.displayName;
        this.email = resultado.email;
        this.foto = resultado.photoURL;
      } else {
        this.isLogin = false;
      }
    });
  }

  salir() {
    console.log('Salir presionado');
    this.usuarioService.logOut();
    this.toastrService.info('Volve Pronto', 'Adios');

  }

}
