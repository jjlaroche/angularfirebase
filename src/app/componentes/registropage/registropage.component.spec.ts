import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistropageComponent } from './registropage.component';

describe('RegistropageComponent', () => {
  let component: RegistropageComponent;
  let fixture: ComponentFixture<RegistropageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistropageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistropageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
