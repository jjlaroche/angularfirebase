import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../../servicios/usuario.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-registropage',
  templateUrl: './registropage.component.html',
  styleUrls: ['./registropage.component.css']
})
export class RegistropageComponent implements OnInit {

  email: any;
  clave: any;

  constructor(private usuarioService: UsuarioService, public router: Router, private toastrService: ToastrService) { }

  ngOnInit() {
  }

  onSubmitLogin() {
    this.usuarioService.registrarUsuario(this.email, this.clave)
    .then((resultado) => {
      this.toastrService.success('Registro exitoso', 'Operacion Exitosa');
      this.router.navigate(['/privado']);
    });

  }



}
