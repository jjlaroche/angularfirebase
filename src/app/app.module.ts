import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ReclamosComponent } from './reclamos/reclamos.component';

import { ReclamosService } from './servicios/reclamos.service';

import { BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { ToastrModule} from 'ngx-toastr';

import { environment } from '../environments/environment';

import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireModule } from 'angularfire2';

import { FormsModule } from '@angular/forms';
import { HomepageComponent } from './componentes/homepage/homepage.component';
import { RegistropageComponent } from './componentes/registropage/registropage.component';
import { NavbarpageComponent } from './componentes/navbarpage/navbarpage.component';
import { LoginComponent } from './componentes/login/login.component';
import { PrivadopageComponent } from './componentes/privadopage/privadopage.component';
import { NotfoundpageComponent } from './componentes/notfoundpage/notfoundpage.component';

import { UsuarioService } from './servicios/usuario.service';

import { Routes, RouterModule } from '@angular/router';

import { AngularFireAuth, AngularFireAuthModule } from 'angularfire2/auth';

import { AuthGuard } from './seguridad/auth.guard';

const routes: Routes = [
                        {path: '', component: HomepageComponent},
                        {path: 'login' , component: LoginComponent},
                        {path: 'registro', component: RegistropageComponent},
                        {path: 'privado', component : PrivadopageComponent, canActivate: [AuthGuard] },
                        {path: 'crud', component: ReclamosComponent},
                        {path: '**', component: NotfoundpageComponent}
];


@NgModule({
  declarations: [
    AppComponent,
    ReclamosComponent,
    HomepageComponent,
    RegistropageComponent,
    NavbarpageComponent,
    LoginComponent,
    PrivadopageComponent,
    NotfoundpageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    AngularFireDatabaseModule,
    AngularFireModule.initializeApp(environment.firebase),
    FormsModule,
    RouterModule.forRoot(routes),
    AngularFireAuthModule
  ],
  providers: [ReclamosService, UsuarioService, AngularFireAuth, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
