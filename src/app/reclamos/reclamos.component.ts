import { Component, OnInit } from '@angular/core';

import { ToastrService } from 'ngx-toastr';

import { ReclamosService } from '../servicios/reclamos.service';

import { Reclamo } from '../Modelos/reclamo';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-reclamos',
  templateUrl: './reclamos.component.html',
  styleUrls: ['./reclamos.component.css']
})
export class ReclamosComponent implements OnInit {

  reclamoLista: Reclamo[];

  constructor(private toastrService: ToastrService, public reclamosService: ReclamosService ) {

  }

  ngOnInit() {
    this.obtenerInformacion();
  }

  obtenerInformacion() {

    let contenido = this.reclamosService.getReclamos();

    contenido.snapshotChanges().subscribe( item => {
      this.reclamoLista = [];

      item.forEach( elemento => {

        let registro = elemento.payload.toJSON();

        registro['$key'] = elemento.key;

        this.reclamoLista.push(registro as Reclamo);

      });
    });


  }

  editar(item: Reclamo) {
    console.log('Boton editar presioando');
    this.reclamosService.selectedReclamo = Object.assign({}, item);
  }

  eliminar(item) {
    console.log('Boton Eliminar presioando');
    if (confirm('Desea eliminar?')) {
      this.reclamosService.eliminarReclamo(item.$key);
      this.toastrService.success('Reclamo eliminado', 'Operacion Exitosa');
    }
  }

  agregar(item) {
    console.log('Boton Agregar presioando');
  }

  onSubmit(reclamoForm: NgForm) {

    console.log('onSubmit presionado' + JSON.stringify(reclamoForm.value));
    if (this.reclamosService.selectedReclamo.$key == null) {
      this.reclamosService.agregarReclamo(reclamoForm.value);

      this.toastrService.success('Reclamo agregado', 'Operacion Exitosa');

    } else {
      this.reclamosService.actualizarReclamo(reclamoForm.value.$key, reclamoForm.value);

      this.toastrService.success('Reclamo modificado', 'Operacion Exitosa');
    }


  }

  resetForm(reclamoForm: NgForm) {
    console.log('resetForm presioando');
    if (reclamoForm != null) {
      reclamoForm.reset();
      this.reclamosService.selectedReclamo = {
        $key: null,
        titulo: '',
        texto: '',
        locacion: ''
      }
    }
  }

}
