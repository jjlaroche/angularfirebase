import { Injectable } from '@angular/core';

import { AngularFireDatabase} from 'angularfire2/database';
import { AngularFireList } from 'angularfire2/database';

import { Reclamo } from '../Modelos/reclamo';
@Injectable({
  providedIn: 'root'
})
export class ReclamosService {

  reclamoList: AngularFireList<any>;
  selectedReclamo: Reclamo = new Reclamo();

  constructor(private angularFireDatabase: AngularFireDatabase) {

  }

  getReclamos() {
    return this.reclamoList = this.angularFireDatabase.list('reclamo');
  }

  getReclamo(identificador: any) {

  }

  agregarReclamo(request: Reclamo) {
    this.reclamoList.push({
      titulo: request.titulo,
      texto: request.texto,
      locacion: request.locacion
    });
  }

  actualizarReclamo(identificador: any, request: Reclamo) {
    this.reclamoList.update(identificador, {
      titulo: request.titulo,
      texto: request.texto,
      locacion: request.locacion
    });
  }

  eliminarReclamo(identificador: any) {
    this.reclamoList.remove(identificador);
  }
}
