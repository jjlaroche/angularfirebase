import { Injectable } from '@angular/core';

import { AngularFireAuth } from 'angularfire2/auth';

import { map } from 'rxjs/operators';

import * as firebase from 'firebase/app';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  constructor(private auth: AngularFireAuth) {

  }

  logOut() {
    return this.auth.auth.signOut();
  }

  registrarUsuario(email: string, clave: string) {
    return new Promise((resolve, reject) => {
      this.auth.auth.createUserWithEmailAndPassword(email, clave)
      .then(resultado => {
        resolve(resultado);
      }, error => {
        reject(error);
      })
    });
  }

  getInfoCuenta() {
    return this.auth.authState.pipe(map(auth => auth));
  }

  loginUsuario(email: string, clave: string) {
    return new Promise((resolve, reject) => {
      this.auth.auth.signInWithEmailAndPassword(email, clave)
      .then(resultado => {
        resolve(resultado);
      }, error => {
        reject(error);
      });
    });
  }

  loginGoogle () {
    return this.auth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider());
  }

  loginFaceBook() {
    return this.auth.auth.signInWithPopup(new firebase.auth.FacebookAuthProvider());
  }
}
